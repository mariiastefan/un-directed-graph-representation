#include "graf.h"

std::vector<Arc> Graf::GetArce()
{
    return arce;
}

std::vector<Node> Graf::GetNoduri()
{
    return noduri;
}

void Graf::AddNod(Node n)
{
    n.setNumber(noduri.size());
    noduri.push_back(n);
}

void Graf::AddArc(Arc n)
{
    arce.push_back(n);
}

Node Graf::GetLastNode()
{
    return noduri[noduri.size()-1];
}

int Graf::getNumberofNodes()
{
    return noduri.size();
}
