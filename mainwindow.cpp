#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "node.h"
#include <iostream>
#include <QMouseEvent>
#include <QPainter>
#include <QtXml>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    drawNode = false;
    drawArc = false;
    isOriented = true;
    ui->setupUi(this);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
    drawNode = false;
    drawArc = false;
    if(e->button() == Qt::RightButton)
    {
        Node n(e->pos());
        g.AddNod(n);
        drawNode = true;
        update();
        firstNode = Node();
    }
    else
    {
        QPointF p = e->localPos();
        std::vector<Node> noduri = g.GetNoduri();
        Node foundNode;
        for (auto& n : noduri)
        {
            if (fabs(n.getPoint().x() - p.x()) < 20 && fabs(n.getPoint().y() - p.y()) < 20)
            {
                foundNode = n;
                break;
            }
        }
        if (foundNode.getNumber() == -1)
            return;
        if (firstNode.getNumber() == -1)
        {
            firstNode = foundNode;
        }
        else
        {
            secondNode = foundNode;
            g.AddArc(Arc(firstNode, secondNode));
            firstNode = Node();
            secondNode = Node();
            drawArc = true;
            update();
        }
    }

}

void MainWindow::paintEvent(QPaintEvent *event)
{

    if (g.getNumberofNodes())
    {
        QPainter p(this);
        std::vector<Node> noduri = g.GetNoduri();
        for(auto& nod: noduri)
        {
            QRect r(nod.getPoint().x()-10,nod.getPoint().y()-10, 20,20);
            p.drawEllipse(r);
            p.drawText(nod.getPoint(), QString(nod.getNumber()));
        }
        std::vector<Arc> arce = g.GetArce();
        for(auto& arc: arce)
        {
            p.drawLine(arc.getFirstPoint().getPoint(), arc.getSecondPoint().getPoint());

        }
        if (drawNode)
        {
            Node n = g.GetLastNode();
            p.setPen(QPen(Qt::red));
            QRect r(n.getPoint().x()-10,n.getPoint().y()-10, 20,20);
            p.drawEllipse(r);
            p.drawText(n.getPoint(), QString(n.getNumber()));
        }
        else if (drawArc)
        {
            p.setPen(QPen(Qt::red));
            p.drawLine(QLine(arce[arce.size()-1].getFirstPoint().getPoint(), arce[arce.size()-1].getSecondPoint().getPoint()));
        }
    }
}



MainWindow::~MainWindow()
{
    delete ui;
}


/*void MainWindow::on_SaveGraf_released()
{

}


void MainWindow::on_Orientat_clicked()
{
    isOriented = true;
}


void MainWindow::on_Neorientat_clicked()
{
    isOriented = false;
}*/

