#ifndef GRAF_H
#define GRAF_H
#include "node.h"
#include "arc.h"

class Graf
{
    std::vector<Node>noduri;
    std::vector<Arc> arce;
    std::vector <std::vector<int>> matriceAdiacenta;
    std::vector <std::vector<Node>> listaAdiacenta;

public:
    void GenerareMatriceAdiacenta();
    void GenerareListaAdiacenta();
    std::vector<Arc> GetArce();
    std::vector<Node> GetNoduri();
    void AddNod(Node n);
    void AddArc(Arc a);
    void DrawGraf(QPainter *p);
    Node GetLastNode();
    int getNumberofNodes();
};

#endif // GRAF_H
